import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SessionItemComponent } from './session-item/session-item.component';
import { SessionItemListComponent } from './session-item-list/session-item-list.component';
import { InscriptiondisabledDirective } from './inscriptiondisabled.directive';
import {RouterModule, Routes} from '@angular/router';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import{MatSidenavModule} from '@angular/material/sidenav';
import {AuthModule} from './admin/auth/auth/auth.module'
import { AuthService } from './admin/auth/auth.service';
const appRoutes: Routes = [
{path: 'list',component: SessionItemListComponent},
{path: 'login',loadChildren: './admin/auth/auth/auth.module#AuthModule'},
{path: 'admin', loadChildren: './admin/admin.module#AdminModule',canActivate :[AuthService]},
{ path: '',   redirectTo: '/list', pathMatch: 'full' },
{ path: '**', component: PagenotfoundComponent }];
@NgModule({
  declarations: [
    AppComponent,
    SessionItemComponent,
    SessionItemListComponent,
    InscriptiondisabledDirective,
    PagenotfoundComponent,
    
  ],
  imports: [
    RouterModule.forRoot
    (appRoutes,{ enableTracing: true, relativeLinkResolution: 'legacy' }),
    BrowserModule,
    BrowserAnimationsModule,
    MatListModule,
            MatDividerModule,
            MatToolbarModule,
            MatIconModule,
            MatButtonModule,
            FlexLayoutModule,
            MatMenuModule,
            MatTableModule,
            MatPaginatorModule,
            MatFormFieldModule,
            MatInputModule,
            MatSidenavModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
