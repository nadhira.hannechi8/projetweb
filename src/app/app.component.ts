import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'projetWeb';
  nbpart :number =0;

  
  constructor() { }

  ngOnInit(): void {
  }
  nbParticipantsChange( event){
    this.nbpart=event.value
  }
}
