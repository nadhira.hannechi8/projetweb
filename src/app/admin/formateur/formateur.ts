
export class Formateur {
        
         public id: number;
         public name: string;
         public lastname: string;
         public address: String;
         public email: String;
         public expertise: string;

         constructor(formateur) {
          this.id = formateur.id;
          this.name= formateur.name;
          this.lastname = formateur.lastname;
          this.address= formateur.address;
          this.email=formateur.email;
          this.expertise=formateur.expertise;
        }
      
       }
   