import icAdd from '@iconify/icons-ic/twotone-add';
import {AfterViewInit, Component, ViewChild,OnInit} from '@angular/core';
import { FormControl } from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import icSearch from '@iconify/icons-ic/twotone-search';
import { MatDialog } from '@angular/material/dialog';
import { FormateurEditComponent } from './formateur-edit/formateur-edit.component';
import {Formateur} from './formateur'
import {ForamteurITEMS} from './formateurs'
import { FackeFormateurService } from './facke-formateur.service';
import { Observable, of, ReplaySubject } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { FormateurCreateComponent } from './formateur-create/formateur-create.component';
@Component({
  selector: 'app-formateur',
  templateUrl: './formateur.component.html',
  styleUrls: ['./formateur.component.css']
})
export class FormateurComponent implements OnInit {
  dataSource= new MatTableDataSource<Formateur>();
  icSearch = icSearch;
    displayedColumns: string[] = ['id', 'name', 'LastName', 'Address','Email','Expertise','actions'];
 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog,private service:FackeFormateurService) {
   
  }
  
  Formateurs=this.service.get();
  ngOnInit(): void {
    this.dataSource=new MatTableDataSource();
    this.dataSource.data=this.Formateurs;

  }
  
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    console.log(this.dataSource)
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  CreateFormateur(){
    this.dialog.open(FormateurCreateComponent, {
      
      width: '600px'
      }).afterClosed().subscribe(newFormateur=> {
        
        if (newFormateur) {
          this.Formateurs=this.service.add(newFormateur)
          this.ngOnInit()
        }
      });
  
    }
  updateFormateur(formateur:Formateur){
    this.dialog.open(FormateurEditComponent, {
      data: formateur,
      width: '600px'
      }).afterClosed().subscribe(updatedFormateur=> {
        
        if (updatedFormateur) {
          const index = this.Formateurs.findIndex((existingFormateur) => existingFormateur.id === updatedFormateur.id);
          this.Formateurs[index] = new Formateur(updatedFormateur)
          this.ngOnInit()
        }
      });
  
    }
   deleteFormateur(formateur:Formateur){
    this.Formateurs=this.service.delete(formateur)
    this.ngOnInit()
  }
}




 
