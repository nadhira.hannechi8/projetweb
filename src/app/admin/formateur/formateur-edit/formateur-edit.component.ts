import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Formateur } from '../formateur';
import { FackeFormateurService } from '../facke-formateur.service';

@Component({
  selector: 'app-formateur-edit',
  templateUrl: './formateur-edit.component.html',
  styleUrls: ['./formateur-edit.component.css']
})
export class FormateurEditComponent implements OnInit {

  
  form: FormGroup;
  
  

  constructor(@Inject(MAT_DIALOG_DATA) public defaults: any,
  private dialogRef: MatDialogRef<FormateurEditComponent>,
   private fb: FormBuilder, private service:FackeFormateurService) {
  }

  ngOnInit() {
    console.log(this.defaults)
   
    this.form = this.fb.group({
     id:this.defaults.id,
     name:this.defaults.name,
     lastname:this.defaults.lastname,
     address:this.defaults.address,
     email:this.defaults.email,
     expertise:this.defaults.expertise
      
      
    });
  }

  save() {
      const formateur = this.form.value;
      formateur.id = this.defaults.id;
      this.dialogRef.close(formateur);
    
  }
 
 
  
}

