import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormateurCreateComponent } from './formateur-create.component';

describe('FormateurCreateComponent', () => {
  let component: FormateurCreateComponent;
  let fixture: ComponentFixture<FormateurCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormateurCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormateurCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
