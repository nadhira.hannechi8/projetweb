import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Formateur } from '../formateur';
import { FackeFormateurService } from '../facke-formateur.service';
@Component({
  selector: 'app-formateur-create',
  templateUrl: './formateur-create.component.html',
  styleUrls: ['./formateur-create.component.css']
})
export class FormateurCreateComponent implements OnInit {

   
  form: FormGroup;
  
  

  constructor(@Inject(MAT_DIALOG_DATA) public defaults: any,
  private dialogRef: MatDialogRef<FormateurCreateComponent>,
   private fb: FormBuilder, private service:FackeFormateurService) {
  }

  ngOnInit() {
   
    this.form = this.fb.group({
     id:null,
     name:null,
     lastname:null,
     address:null,
     email:null,
     expertise:null
      
      
    });
  }

  save() {
    if(this.form.value.name!=null && this.form.value.lastname!=null && this.form.value.address!=null && this.form.value.email!=null && this.form.value.expertise!=null){
      const formateur = this.form.value;
      this.dialogRef.close(formateur);
    }
    }
 
 
  
}