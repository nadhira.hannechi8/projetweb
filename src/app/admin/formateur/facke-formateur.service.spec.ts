import { TestBed } from '@angular/core/testing';

import { FackeFormateurService } from './facke-formateur.service';

describe('FackeFormateurService', () => {
  let service: FackeFormateurService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FackeFormateurService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
