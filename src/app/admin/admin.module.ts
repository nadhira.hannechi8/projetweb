import { NgModule } from '@angular/core';
import { AdminComponent } from './admin/admin.component';
import { SessionItemComponent } from './session-item/session-item.component';
import { SessionItemListComponent } from './session-item-list/session-item-list.component';
import { SessionAddFormComponent } from './session-add-form/session-add-form.component';
import { SessionEditFormComponent } from './session-edit-form/session-edit-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FakeSessionitemService } from './fake-sessionitem.service';
import { Router, RouterModule, Routes } from '@angular/router';
import { DefaultComponent } from './default/default.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import{MatSidenavModule} from '@angular/material/sidenav'
import { DashbordComponent } from './dashbord/dashbord.component';
import { CommonModule } from '@angular/common';
import { FormateurComponent } from './formateur/formateur.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { IconModule } from '@visurel/iconify-angular';
import { FormateurEditComponent } from './formateur/formateur-edit/formateur-edit.component';
import { FormateurCreateComponent } from './formateur/formateur-create/formateur-create.component';
import { LoginComponent } from './auth/login/login.component';
import { ParticipantComponent } from './participant/participant.component';
import { ParticipantCreateComponent } from './participant/participant-create/participant-create.component';
import { ParticipantEditComponent } from './participant/participant-edit/participant-edit.component';

const adminRoutes: Routes = [
  {
   path: '', component: DefaultComponent,
   children: [
    {  path: 'dashbord', component: DashbordComponent },
    {  path: 'session', component: AdminComponent ,
    children: [
      { path: 'add', component: SessionAddFormComponent },
      { path: 'edit/:id', component: SessionEditFormComponent },
      { path: 'list', component:SessionItemListComponent },
      { path:'', redirectTo:'list'}
    ]},
    {  path: 'formateur', component: FormateurComponent },
    {  path: 'participant', component: ParticipantComponent },
      { path: '',   redirectTo: 'dashbord'}]
  }];
  
@NgModule({
    declarations: [
      AdminComponent,
      SessionItemComponent,
      SessionItemListComponent,
      SessionAddFormComponent,
      SessionEditFormComponent,
      DefaultComponent,
      FooterComponent,
      SidebarComponent,
      HeaderComponent,
      DashbordComponent,
      FormateurComponent,
      FormateurEditComponent,
      FormateurCreateComponent,
      ParticipantComponent,
      ParticipantCreateComponent,
      ParticipantEditComponent],
          imports: [
            CommonModule,
            FormsModule,
            RouterModule.forChild(adminRoutes),
            RouterModule,
            MatListModule,
            MatDividerModule,
            MatToolbarModule,
            MatIconModule,
            MatButtonModule,
            FlexLayoutModule,
            MatMenuModule,
            MatTableModule,
            MatPaginatorModule,
            MatFormFieldModule,
            MatInputModule,
            MatSidenavModule,
            CommonModule,
            ReactiveFormsModule,
            FlexLayoutModule,
            MatDialogModule,
            MatInputModule,
            MatButtonModule,
            MatIconModule,
            MatRadioModule,
            MatSelectModule,
            MatMenuModule,
            IconModule,
            MatDividerModule
           
           
            /*MatCheckboxModule,
            DynamicTableModule,
            NgxPaginationModule,
            Ng2SearchPipeModule,
            MatSortModule*/

    
          ],
          providers: [FakeSessionitemService],
          bootstrap: [AdminComponent]
        })
export class AdminModule { }
