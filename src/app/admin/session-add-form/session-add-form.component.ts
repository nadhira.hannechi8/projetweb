import { Component, OnInit } from '@angular/core';
import { FakeSessionitemService } from '../fake-sessionitem.service';
import { Session } from '../session';

@Component({
  selector: 'app-session-add-form',
  templateUrl: './session-add-form.component.html',
  styleUrls: ['./session-add-form.component.css']
})
export class SessionAddFormComponent implements OnInit {

  constructor(private sessionItemService: FakeSessionitemService) { }
  sessionItem: Session
  ngOnInit(): void {
  }
  addSession(sessionItem) {
    this.sessionItemService.add(sessionItem);
    }
      
}
