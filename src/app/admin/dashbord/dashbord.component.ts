import { Component, OnInit } from '@angular/core';
import { FakeSessionitemService } from '../fake-sessionitem.service';
import { FackeFormateurService } from '../formateur/facke-formateur.service';
import { ForamteurITEMS } from '../formateur/formateurs';
import { ParticipantITEMS } from '../participant/participants';

@Component({
  selector: 'app-dashbord',
  templateUrl: './dashbord.component.html',
  styleUrls: ['./dashbord.component.css']
})
export class DashbordComponent implements OnInit {
  sessionItems;
  formateurs;
  lgParticipant;
  lgFormateur;
  lgSession;
  constructor(private sessionItemsService: FakeSessionitemService, private FormateurService :FackeFormateurService) { }

  ngOnInit(): void {
    this.sessionItems = this.sessionItemsService.get();
    this.formateurs = this.FormateurService.get().slice(0,2);
    this.lgParticipant = ParticipantITEMS.length;
    this.lgFormateur = ForamteurITEMS.length;
    this.lgSession = this.sessionItems.length;
  }
  countSession(track : string)
  {
   let resultat = 0;
   this.sessionItems.forEach(i=>
    {
       if (i.track === track)
       {
         resultat++;
       }
    });
   
    return resultat;
  }
 

}
