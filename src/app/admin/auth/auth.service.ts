import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router) { }
  public getTokenFromLocalStorage(): string {
  
    return localStorage.getItem('password');
}

  public isAuthenticated(): boolean {
    const token = this.getTokenFromLocalStorage();
    return token == null;
  }

  canActivate(): boolean {
    if (this.isAuthenticated()){
      this.router.navigate(['']);
      return false;
    }
    return true;
  }
  
}
