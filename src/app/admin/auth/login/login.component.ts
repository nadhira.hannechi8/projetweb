import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import icVisibility from '@iconify/icons-ic/twotone-visibility';
import icVisibilityOff from '@iconify/icons-ic/twotone-visibility-off';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

 
  form = this.fb.group({
    email:null,
    password:null
  });
  
  inputType = 'password';
  visible = false;

  icVisibility = icVisibility;
  icVisibilityOff = icVisibilityOff;

  constructor(private router: Router,
              private fb: FormBuilder,
              private cd: ChangeDetectorRef,
              private snackbar: MatSnackBar
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });


   
  }

  
  send() {
    const email=this.form.get('email').value
    const password=this.form.get('password').value

    if((email=="nadhira@gmail.com")&&(password=="admin"))
    {
      this.router.navigate(['admin']);
      this.snackbar.open('Lucky you! Welcome to Our Dashbord ;)','', {
        duration: 500
      });
      localStorage.setItem('password', password);
    }
    else
    {
      this.snackbar.open('Please verify your information !!!','', {
        duration: 10000
      });
    }
    

  }



 
}
