import icAdd from '@iconify/icons-ic/twotone-add';
import {AfterViewInit, Component, ViewChild,OnInit} from '@angular/core';
import { FormControl } from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import icSearch from '@iconify/icons-ic/twotone-search';
import { MatDialog } from '@angular/material/dialog';
import { ParticipantEditComponent } from './participant-edit/participant-edit.component';
import {Participant} from './participant'
import {ParticipantITEMS} from './participants'
import {FakeParticipantService } from './fake-participant.service';
import { Observable, of, ReplaySubject } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ParticipantCreateComponent } from './participant-create/participant-create.component';
@Component({
  selector: 'app-participant',
  templateUrl: './participant.component.html',
  styleUrls: ['./participant.component.css']
})
export class ParticipantComponent implements OnInit {

  dataSource= new MatTableDataSource<Participant>();
  icSearch = icSearch;
    displayedColumns: string[] = ['id', 'name', 'LastName', 'Address','Email','Expertise','actions'];
 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog,private service:FakeParticipantService) {
   
  }
  
  Participants=this.service.get();
  ngOnInit(): void {
    this.dataSource=new MatTableDataSource();
    this.dataSource.data=this.Participants;

  }
  
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    console.log(this.dataSource)
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  CreateParticipant(){
    this.dialog.open(ParticipantCreateComponent, {
      
      width: '600px'
      }).afterClosed().subscribe(newParticipant=> {
        
        if (newParticipant) {
          this.Participants=this.service.add(newParticipant)
          this.ngOnInit()
        }
      });
  
    }
  updateParticipant(participant:Participant){
    this.dialog.open(ParticipantEditComponent, {
      data: participant,
      width: '600px'
      }).afterClosed().subscribe(updatedParticipant=> {
        
        if (updatedParticipant) {
          const index = this.Participants.findIndex((existingParticipant) => existingParticipant.id === updatedParticipant.id);
          this.Participants[index] = new Participant(updatedParticipant)
          this.ngOnInit()
        }
      });
  
    }
   deleteParticipant(participant:Participant){
    this.Participants=this.service.delete(participant)
    this.ngOnInit()
  }
}




 
