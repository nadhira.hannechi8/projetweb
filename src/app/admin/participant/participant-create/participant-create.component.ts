import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FakeParticipantService } from '../fake-participant.service';

@Component({
  selector: 'app-participant-create',
  templateUrl: './participant-create.component.html',
  styleUrls: ['./participant-create.component.css']
})
export class ParticipantCreateComponent implements OnInit {

  form: FormGroup;
  
  

  constructor(@Inject(MAT_DIALOG_DATA) public defaults: any,
  private dialogRef: MatDialogRef<ParticipantCreateComponent>,
   private fb: FormBuilder, private service:FakeParticipantService) {
  }

  ngOnInit() {
   
    this.form = this.fb.group({
     id:null,
     name:null,
     lastname:null,
     address:null,
     email:null,
     expertise:null
      
      
    });
  }

  save() {
      if(this.form.value.name!=null && this.form.value.lastname!=null && this.form.value.address!=null && this.form.value.email!=null && this.form.value.expertise!=null){
      const participant = this.form.value;
      this.dialogRef.close(participant);
      }
    }
 
 
  
}