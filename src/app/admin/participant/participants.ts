import { Participant } from "./participant";

export const ParticipantITEMS: Participant[] = [
    {
        id: 1,
        name: 'Sami',
        lastname: 'Mhadhbi',
        address: 'Monastir , Tunisie',
        email:"sami.mhadhbi@gmail.com",
        expertise: 'Web Developement',

        },

        {
        id: 2,
        name: 'Abir',
        lastname: 'Lakhal',
        address: 'Sousse , Tunisie',
        email:"abir.Lakhal@gmail.com",
        expertise: 'Web Developement',
                
        },
        {
        id: 3,
        name: 'Arij',
        lastname: 'Othmeni',
        address: 'Tunis , Tunisie',
        email:"Arij.othmeni@gmail.com",
        expertise: 'Mobile Developement',
    
        },
        {
        id: 4,
        name: 'Fathi',
        lastname: 'Kileni',
        address: 'Tunis , Tunisie',
        email:"fathi@gmail.com",
        expertise: 'Web Developement',
        
        },
        
        {
        id: 5,
        name: 'Lobna',
        lastname: 'sehli',
        address: 'Gafsa , Tunisie',
        email:"lobna.sehli@gmail.com",
        expertise: 'Mobile Developement',
                
        },
        {
        id: 6,
        name: 'Nawel',
        lastname: 'Taher',
        address: 'Mednine, Tunisie',
        email:"Nawel.taher@gmail.com",
        expertise: 'Web Developement',
                    
        },
                    
];