import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FakeParticipantService } from '../fake-participant.service';

@Component({
  selector: 'app-participant-edit',
  templateUrl: './participant-edit.component.html',
  styleUrls: ['./participant-edit.component.css']
})
export class ParticipantEditComponent implements OnInit {

  form: FormGroup;
  
  

  constructor(@Inject(MAT_DIALOG_DATA) public defaults: any,
  private dialogRef: MatDialogRef<ParticipantEditComponent>,
   private fb: FormBuilder, private service:FakeParticipantService) {
  }

  ngOnInit() {
    console.log(this.defaults)
   
    this.form = this.fb.group({
     id:this.defaults.id,
     name:this.defaults.name,
     lastname:this.defaults.lastname,
     address:this.defaults.address,
     email:this.defaults.email,
     expertise:this.defaults.expertise
      
      
    });
  }

  save() {
    
      const participant = this.form.value;
      participant.id = this.defaults.id;
      this.dialogRef.close(participant);
    
    }
 
 
  
}

