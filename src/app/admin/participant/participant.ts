export class Participant {
        
    public id: number;
    public name: string;
    public lastname: string;
    public address: String;
    public email: String;
    public expertise: string;

    constructor(participant) {
     this.id = participant.id;
     this.name= participant.name;
     this.lastname = participant.lastname;
     this.address= participant.address;
     this.email=participant.email;
     this.expertise=participant.expertise;
   }
 
  }