import { Injectable } from '@angular/core';
import {Participant} from './participant'
import {ParticipantITEMS} from './participants'
@Injectable({
  providedIn: 'root'
})
export class FakeParticipantService {

  constructor() { }
  get() { 
    return ParticipantITEMS; 
  }

  add(participantItem) {
    participantItem.id = ParticipantITEMS.length + 1;
    ParticipantITEMS.push(participantItem);
    return ParticipantITEMS;
  }
  delete(participantItem) {
    let index;
    index = ParticipantITEMS.indexOf(participantItem);
    if (ParticipantITEMS.indexOf(participantItem) >= 0) {
      ParticipantITEMS.splice(index, 1);
      return ParticipantITEMS;
  }
}
  update(id,participantItem){

    console.log(id)
    ParticipantITEMS[id -1]=participantItem;
    console.log(ParticipantITEMS[id -1])
  }
  
  getSession(id: number) 
  {
    return ParticipantITEMS[id -1];
  }
}
