import { Component, OnInit } from '@angular/core';
import { FakeSessionitemService } from '../fake-sessionitem.service';

@Component({
  selector: 'app-session-item-list',
  templateUrl: './session-item-list.component.html',
  styleUrls: ['./session-item-list.component.css']
})
export class SessionItemListComponent implements OnInit {
 sessionItems;
  constructor(private sessionItemService: FakeSessionitemService) { }

  ngOnInit(): void {
    this.sessionItems=this.sessionItemService.get();
  }

}
